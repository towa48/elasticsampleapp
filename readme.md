# Sample application with logging to ElasticSearch from .NET Core

## Requirements

* [http://dotnet.github.io/docs/getting-started/installing/installing-core-windows.html](.NET Core & DNVM)

## How to run

```powershell
dnu restore
dnu build
dnx ElasticApp
```
