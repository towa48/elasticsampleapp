using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ElasticApp
{
  public class ElasticClient
  {
    private readonly string _url;

    public ElasticClient(string url)
    {
      _url = url;
    }

    public void Index(string index, string type, string content)
    {
      // HTTP POST http://logsrv:9200/{index}/{type}
      // HTTP POST http://logsrv:9200/log-2016.01.24/ElasticApp
      System.Net.Http.HttpClient client = null;
      try
      {
        client = new System.Net.Http.HttpClient();
        client.BaseAddress = new Uri(_url);
        var result = client.PostAsync(string.Format("/{0}/{1}", index, type), new StringContent(content)).Result;
        string resultContent = result.Content.ReadAsStringAsync().Result;
        Console.WriteLine(resultContent);
      }
      catch(Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
      finally
      {
        if (client != null)
        {
          client.Dispose();
        }
      }
    }
  }
}
