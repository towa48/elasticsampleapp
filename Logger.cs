using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ElasticApp
{
  public class Logger
  {
    private readonly string _name;
    private readonly string _index;
    private readonly string _filePath;
    private readonly ElasticClient _elastic;

    public Logger(string name)
    {
      _name = name;
      _index = "log-{0:yyyy.MM.dd}";

      //var location = typeof(Logger).GetTypeInfo().Assembly.Location;
      //var location = System.Reflection.Assembly.GetExecutingAssembly().Location;
      //var location = AppDomain.CurrentDomain.BaseDirectory;
      _filePath = Path.Combine("logs", _name + ".log");

      _elastic = new ElasticClient("http://logsrv:9200");
    }

    public void Error(Exception ex)
    {
      Write(new LogEvent()
      {
        Level = LogLevel.Error,
        TimeStamp = DateTime.UtcNow,
        Exception = ex,
        Message = ex.ToString()
      });
    }

    public void Error(string message)
    {
      Write(new LogEvent()
      {
        Level = LogLevel.Error,
        TimeStamp = DateTime.UtcNow,
        Message = message
      });
    }

    private void Write(LogEvent logEvent)
    {
      WriteToFile(logEvent);
      WriteToElastic(logEvent);
    }

    private void WriteToFile(LogEvent logEvent)
    {
      var document = new List<string>();
      document.Add(logEvent.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
      document.Add(logEvent.Level.Name);
      document.Add(_name);
      document.Add(logEvent.Message);

      var sb = new StringBuilder();
      for(var i=0;i<document.Count;i++)
      {
        sb.Append(document[i]);
        if (i != document.Count-1) {
          sb.Append("|");
        }
      }
      sb.Append(Environment.NewLine);

      System.IO.File.AppendAllText(_filePath, sb.ToString());
    }

    private void WriteToElastic(LogEvent logEvent)
    {
      var document = new Dictionary<string, object>();
      document.Add("@timestamp", logEvent.TimeStamp);
      document.Add("level", logEvent.Level.Name);
      if (logEvent.Exception != null)
      {
        document.Add("exception", logEvent.Exception.ToString());
      }
      document.Add("message", logEvent.Message);

      _elastic.Index(string.Format(_index, logEvent.TimeStamp), _name, JsonConvert.SerializeObject(document));
    }
  }

  class LogEvent
  {
    public DateTime TimeStamp { get; set; }
    public LogLevel Level { get; set; }
    public Exception Exception { get; set; }
    public string Message { get; set; }
  }

  sealed class LogLevel
  {
    public static readonly LogLevel Error = new LogLevel("ERROR");

    public LogLevel(string name)
    {
      Name = name;
    }

    public string Name { get; set; }
  }
}
